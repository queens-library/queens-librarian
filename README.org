#+TITLE: Queens Librarian
#+DESCRIPTION: A .NET Backend to be used in conjunction with the Queens Library.
#+AUTHOR: Damien Rodriguez
#+STARTUP: showeverything

* About Queens Librarian
The Queens Librarian is a .NET Backend that interacts with a SQLite Database provided by [[https://mtgjson.com]]
If you wish to work with this yourself, you will need to grab the SQLite database from the website in order to get the backend to work properly.
